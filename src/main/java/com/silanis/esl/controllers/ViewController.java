package com.silanis.esl.controllers;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.silanis.esl.models.NotificationEventsType;

//Displays a page indicating successful or incomplete signing depending on the package status 
public class ViewController extends HttpServlet {
	private static final long	serialVersionUID	= 1L;
	private static String		Success				= "/WEB-INF/AutoSubmitOnComplete.jsp";
	private static String		Incomplete			= "/WEB-INF/AutoSubmitOnOther.jsp";

	public ViewController() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forward = "";
		String notificationName = request.getParameter("status");

		if (notificationName.contentEquals(NotificationEventsType.PACKAGE_COMPLETE.toString())
				|| notificationName.contentEquals(NotificationEventsType.SIGNER_COMPLETE.toString())) {
			forward = Success;
		}
		else {
			forward = Incomplete;
		}

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}
}
