package com.silanis.esl.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.silanis.esl.models.InsuranceFormModel;
import com.silanis.esl.sdk.PackageId;
import com.silanis.esl.sdk.SessionToken;
import com.silanis.esl.services.ESLPackageCreation;

//Displays the "Insurance Form Page" of the insurance company web application  
public class InsuranceFormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String ESLSIGNATURE_JSP = "/WEB-INF/ESLSignature.jsp"; 

		public InsuranceFormController() {
		super();
	}
	

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/InsuranceForm.jsp");
		view.forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// Get the request parameters from the Insurance Form
		String insuredInitials = request.getParameter("insuredInitials");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String address = request.getParameter("address");
		String city = request.getParameter("city");
		String province = request.getParameter("province");
		String emailAddress = request.getParameter("emailAddress");
		String vehicleModel = request.getParameter("vehicleModel");
		String vehicleMake = request.getParameter("vehicleMake");
		String vehicleModelYear = request.getParameter("vehicleModelYear");
		String vehicleColor = request.getParameter("vehicleColor");

		// Create object

		InsuranceFormModel form = new InsuranceFormModel();
		form.setInsuredInitials(insuredInitials); 
		form.setFirstName(firstName);
		form.setLastName(lastName);
		form.setAddress(address);
		form.setCity(city);
		form.setProvince(province);
		form.setEmailAddress(emailAddress);
		form.setVehicleModel(vehicleModel);
		form.setVehicleMake(vehicleMake);
		form.setVehicleModelYear(vehicleModelYear);
		form.setVehicleColor(vehicleColor);
		
		HttpSession session = request.getSession();
		String eslApiKey = (String)session.getAttribute("eslApiKey");
		String eslApiUrl = (String)session.getAttribute("eslApiUrl");
		String autoSubmitUrl = (String)session.getAttribute("autoSubmitUrl");
        
		// Create a packageId    
		PackageId packageId = ESLPackageCreation.createPackage(form, eslApiKey, eslApiUrl, autoSubmitUrl);
		
		//Get the session token 
		SessionToken token = ESLPackageCreation.createSessionToken(packageId,"signer1", eslApiKey, eslApiUrl);
		
		
		// Redirect the signer to e-SignLive
		String forward = ESLSIGNATURE_JSP;
		request.setAttribute("token", token);
	    RequestDispatcher view = request.getRequestDispatcher(forward);
	    view.forward(request, response);
	}
	
}